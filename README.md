# Strupler2013d- Supplement Data

Data, preprint and printed version of the article **Strupler 2013**, Vorratshaltung im
mittelbronzezeitlichen Bogazköy. Spiegel einer häuslichen und regionalen
Ökonomie, *Istanbuler Mitteilungen*, 2013, 63, 17-50




## LaTeX Preprint

This is the last draft of my article before the conversion into in a Microsoft
Word Document for the publication. In this draft, I already included the
comments of the *Istanbuler Mitteilungen* editorial
board. The published version is slightly different with mostly grammatical
corrections. Do not use this version for citation.

It was written in LaTeX using xelatex and BibLaTeX.

### Organisation

  - 0-Master.tex is the master file. Compile it with xelatex and the comments in
    this file explains what are the other files.
  - the folder **images**
  - bibliography.bib 


## Data

Data used for the paper are divided in 4 folders:
   
  - **capacity-tables**: measurements of the ceramic. There is a
     specific *readme* in the folder explaining the table
  - **drawings**: images used to measure the ceramic 
  - **rcode**: code and data for the plot of the fig. 8
  - **svg**: the vector file used to process the figures (ceramic and plans)

 
## Citation of the article	
 
Strupler Néhémie, Vorratshaltung im mittelbronzezeitlichen Bogazköy. Spiegel
einer häuslichen und regionalen Ökonomie, Istanbuler Mitteilungen, 2013, 63,
17-50

### BibTeX citation

    @Article{Strupler2013d,
    Title    = {Vorratshaltung im mittelbronzezeitlichen Bogazköy. Spiegel
                einer häuslichen und regionalen Ökonomie},
    Author   = {Néhémie Strupler},
    Journal  = {Istanbuler Mitteilungen},
    Year     = {2013},
    Pages    = {17-50},
    Volume   = {63}
    }

## Citation of this repository (and data)

If you would like to cite this data (in case you use it), I will publish it with
Zenodo to obtain a **DOI**. Nobody comments on this repository, and I would like
to have some critics to improve it (or the English) before the archiving. Feel free
to contact me!
